#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Bitmap
{
public:
	Bitmap(std::string filename)
	{
		FILE *image = fopen(filename.c_str(), "rb");
		is_valid = image != NULL;
		if (!is_valid) return;

		fseek(image, 0, SEEK_END);
		size = ftell(image);
		is_valid = size >= 54;
		if (!is_valid) return;

		fseek(image, 0, SEEK_SET);
		fread(&sig, sizeof (short), 1, image);
		is_valid = sig == 0x4d42;
		if (!is_valid) return;

		fseek(image, 18, SEEK_SET);
		fread(&width, sizeof (int), 1, image);
		fseek(image, 22, SEEK_SET);
		fread(&height, sizeof (int), 1, image);
		fseek(image, 28, SEEK_SET);
		fread(&color_depth, sizeof (short), 1, image);
	}
	
	~Bitmap()
	{
		if (image != NULL) fclose(image);
	}
	
	bool IsValid()
	{
		return is_valid;
	}

	uint32_t GetWidth()
	{
		return width;
	}

	uint32_t GetHeight()
	{
		return height;
	}

	uint32_t GetColorDepth()
	{
		return color_depth;
	}

private:
	bool is_valid;
	FILE *image;
	short sig;
	int width, height;
	short color_depth;
	long size;
};
