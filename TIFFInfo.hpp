#pragma once
#include <tiffio.h>
#include <cstdio>
#include "FormatInfo.hpp"

class TIFFInfo : public FormatInfo {
public:
	TIFFInfo()
	{
		TIFFSetErrorHandler(error);
	}

	~TIFFInfo()
	{
	}

	virtual std::string GetFormatName()
	{
		return "tiff";
	}

	virtual void Open(std::string filename)
	{
		is_valid = false;
		image = TIFFOpen(filename.c_str(), "rb");
		is_valid = image != NULL;
	}

	virtual void Close()
	{
		if (image != NULL) TIFFClose(image);
	}

	virtual bool TryRecognize()
	{
		return is_valid;
	}

	virtual int GetWidth()
	{
		int width;
		TIFFGetField(image, TIFFTAG_IMAGEWIDTH, &width);
		return width;
	}

	virtual int GetHeight()
	{
		int height;
		TIFFGetField(image, TIFFTAG_IMAGELENGTH, &height);
		return height;
	}

	virtual int GetColorDepth()
	{
		short bits_per_sample;
		TIFFGetField(image, TIFFTAG_BITSPERSAMPLE, &bits_per_sample);
		return bits_per_sample;
	}

	virtual std::string GetCompressionType()
	{
		std::stringstream ss;
		short compression_type;
		TIFFGetField(image, TIFFTAG_COMPRESSION, &compression_type);
		ss << compression_type << " (tiff tag)";
		return ss.str();
	}

	virtual std::string GetFormatSpecificInfo()
	{
		return "";
	}

private:
	TIFF *image;
	bool is_valid = false;

	static void error(const char *module, const char *fmt, va_list ap)
	{
		return;
	}
};
