#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class PCX
{
public:
	PCX(std::string filename)
	{
		FILE *image = fopen(filename.c_str(), "rb");
		is_valid = image != NULL;
		if (!is_valid) return;

		fseek(image, 0, SEEK_END);
		size = ftell(image);
		is_valid = size >= 128;
		if (!is_valid) return;

		fseek(image, 0, SEEK_SET);
		fread(&sig, sizeof (char), 1, image);
		is_valid = sig == 10;
		if (!is_valid) return;

		short x1, y1, x2, y2;
		fseek(image, 3, SEEK_SET);
		fread(&color_depth, sizeof (char), 1, image);
		fread(&x1, sizeof (short), 1, image);
		fread(&y1, sizeof (short), 1, image);
		fread(&x2, sizeof (short), 1, image);
		fread(&y2, sizeof (short), 1, image);
		fread(&x_dpi, sizeof (short), 1, image);
		fread(&y_dpi, sizeof (short), 1, image);
		width = x2 - x1 + 1;
		height = y2 - y1 + 1;
	}
	
	~PCX()
	{
		if (image != NULL) fclose(image);
	}
	
	bool IsValid()
	{
		return is_valid;
	}
	
	short GetWidth()
	{
		return width;
	}

	short GetHeight()
	{
		return height;
	}

	char GetColorDepth()
	{
		return color_depth;
	}
	
	short GetXDPI()
	{
		return x_dpi;		
	}

	short GetYDPI()
	{
		return y_dpi;		
	}

private:
	bool is_valid;
	FILE *image;
	char sig;
	short width, height, x_dpi, y_dpi;
	char color_depth;
	long size;
};

