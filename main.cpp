#include <iostream>
#include <vector>
#include <memory>
#include "JpegInfo.hpp"
#include "FarbfeldInfo.hpp"
#include "PNGInfo.hpp"
#include "TIFFInfo.hpp"
#include "GIFInfo.hpp"
#include "BitmapInfo.hpp"
#include "PCXInfo.hpp"
#include "AVIFInfo.hpp"

std::vector<std::unique_ptr<FormatInfo>> format_infos;

void print_help(char *argv0)
{
	std::cerr << "usage: " << argv0 << " image\n";
	std::cerr << "supported image formats:";
	for (auto &fi : format_infos)
	{
		std::cerr << " " << fi->GetFormatName();
	}
	std::cerr << std::endl;
	exit(1);
}

int main(int argc, char **argv)
{
	format_infos.push_back(std::make_unique<JpegInfo>());
	format_infos.push_back(std::make_unique<FarbfeldInfo>());
	format_infos.push_back(std::make_unique<PNGInfo>());
	format_infos.push_back(std::make_unique<TIFFInfo>());
	format_infos.push_back(std::make_unique<GIFInfo>());
	format_infos.push_back(std::make_unique<BitmapInfo>());
	format_infos.push_back(std::make_unique<PCXInfo>());
	format_infos.push_back(std::make_unique<AVIFInfo>());

	char *argv0 = argv[0]; --argc; ++argv;
	char *filename = argv[0]; --argc; ++argv;

	if (argc != 0)
	{
		print_help(argv0);
	}

	bool recognized = false;
	for (auto &fi : format_infos)
	{
		fi->Open(filename);
		if (fi->TryRecognize())
		{
			recognized = true;
			std::cout << "format    \t" << fi->GetFormatName() << "\n";
			std::cout << "dimensions\t" <<
			fi->GetWidth() << "x" << fi->GetHeight() << "\n";
			std::cout << "bit depth \t" << fi->GetColorDepth() << "\n";
			std::cout << "compression\t" << fi->GetCompressionType() << "\n";
			std::cout << fi->GetFormatSpecificInfo();
		}
		fi->Close();
		if (recognized) break;
	}
	if (!recognized) {
		std::cerr << "image format is not recognized" << std::endl;
		return 2;
	}
	return 0;
}
