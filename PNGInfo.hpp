#pragma once
#include <png.h>
#include <cstdio>
#include "FormatInfo.hpp"

class PNGInfo : public FormatInfo {
public:
	PNGInfo()
	{
		png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
			NULL, NULL, NULL);
		info_ptr = png_create_info_struct(png_ptr);
	}

	~PNGInfo()
	{
		png_destroy_info_struct(png_ptr, &info_ptr);
		png_destroy_read_struct(&png_ptr, NULL, NULL);
	}

	virtual std::string GetFormatName()
	{
		return "png";
	}

	virtual void Open(std::string filename)
	{
		image = fopen(filename.c_str(), "rb");
		is_valid = image != NULL;
		if (!is_valid) return;

		sig = reinterpret_cast<png_bytep>(new char(8));
		is_valid = fread(sig, 1, 8, image) == 8;
		if (!is_valid) return;

		is_valid = !png_sig_cmp(sig, 0, 8);
		if (!is_valid) return;

		rewind(image);
		png_init_io(png_ptr, image);
		png_read_info(png_ptr, info_ptr);
		png_get_IHDR(png_ptr, info_ptr, &width, &height,
			&bit_depth, &color_type, &interlace_type, NULL, NULL);
	}

	virtual void Close()
	{
		if (image != NULL) fclose(image);
		if (sig != NULL) delete [] sig;
		is_valid = false;
	}

	virtual bool TryRecognize()
	{
		return is_valid;
	}

	virtual int GetWidth()
	{
		return width;
	}

	virtual int GetHeight()
	{
		return height;
	}

	virtual int GetColorDepth()
	{
		return bit_depth;
	}

	virtual std::string GetCompressionType()
	{
		return "lossless";
	}

	virtual std::string GetFormatSpecificInfo()
	{
		std::stringstream ss;
		png_uint_32 x_dpi = png_get_x_pixels_per_inch(png_ptr, info_ptr);
		png_uint_32 y_dpi = png_get_y_pixels_per_inch(png_ptr, info_ptr);
		ss << "X DPI     \t" << x_dpi << "\n";
		ss << "Y DPI     \t" << y_dpi << "\n";
		return ss.str();
	}

private:
	FILE *image;
	png_structp png_ptr;
	png_infop info_ptr;
	bool is_valid = false;
	png_bytep sig;

	png_uint_32 width, height;
	int bit_depth, color_type, interlace_type, compression_type, filter_type;
};
