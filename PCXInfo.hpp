#pragma once
#include <memory>
#include "FormatInfo.hpp"
#include "PCX.hpp"

class PCXInfo : public FormatInfo {
public:
	PCXInfo()
	{
	}

	virtual std::string GetFormatName()
	{
		return "pcx";
	}

	virtual void Open(std::string filename)
	{
		image = std::make_unique<PCX>(filename);
	}

	virtual void Close()
	{
		image.release();
	}

	virtual bool TryRecognize()
	{
		return image->IsValid();
	}

	virtual int GetWidth()
	{
		return image->GetWidth();
	}

	virtual int GetHeight()
	{
		return image->GetHeight();
	}

	virtual int GetColorDepth()
	{
		return image->GetColorDepth();
	}

	virtual std::string GetCompressionType()
	{
		return "lossless";
	}

	virtual std::string GetFormatSpecificInfo()
	{
		std::stringstream ss;
		ss << "X DPI     \t" << image->GetXDPI() << "\n";
		ss << "Y DPI     \t" << image->GetXDPI() << "\n";
		return ss.str();
	}

private:
	std::unique_ptr<PCX> image;
};
