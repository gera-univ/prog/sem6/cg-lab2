#pragma once
#include <tiffio.h>
#include <cstdio>
#include "gifdec/gifdec.h"

class GIFInfo : public FormatInfo {
public:
	GIFInfo()
	{
	}

	~GIFInfo()
	{
	}

	virtual std::string GetFormatName()
	{
		return "gif";
	}

	virtual void Open(std::string filename)
	{
		is_valid = false;
		image = gd_open_gif(filename.c_str());
		is_valid = image != NULL;
	}

	virtual void Close()
	{
		if (image != NULL) gd_close_gif(image);
	}

	virtual bool TryRecognize()
	{
		return is_valid;
	}

	virtual int GetWidth()
	{
		return image->width;
	}

	virtual int GetHeight()
	{
		return image->height;
	}

	virtual int GetColorDepth()
	{
		return image->depth;
	}

	virtual std::string GetCompressionType()
	{
		return "lossless";
	}

	virtual std::string GetFormatSpecificInfo()
	{
		std::stringstream ss;
		ss << "palette size\t" << image->palette->size << "\n";
		return ss.str();
	}

private:
	gd_GIF *image;
	bool is_valid = false;
};
