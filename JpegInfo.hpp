#pragma once
#include <jpeglib.h>
#include <csetjmp>
#include <sstream>
#include "FormatInfo.hpp"

struct MyJpegErrorManager {
    struct jpeg_error_mgr pub;
    jmp_buf setjmp_buffer;
};

class JpegInfo : public FormatInfo {
public:
	JpegInfo()
	{
		cinfo.err = jpeg_std_error(&jerr.pub);
		jerr.pub.error_exit = jpeg_error_exit_ignore;
		jpeg_create_decompress(&cinfo);
	}

	~JpegInfo()
	{
		jpeg_destroy_decompress(&cinfo);
	}

	virtual std::string GetFormatName()
	{
		return "jpg";
	}

	virtual void Open(std::string filename)
	{
		is_valid = false;
		image = fopen(filename.c_str(), "rb");
		is_valid = image != NULL;
		if (!is_valid) return;

		if (setjmp(jerr.setjmp_buffer)) {
			is_valid = false;
			return;
		}
		jpeg_stdio_src(&cinfo, image);
		is_valid = jpeg_read_header(&cinfo, TRUE) == JPEG_HEADER_OK;
		if (!is_valid) return;
	}

	virtual void Close()
	{
		if (image != NULL) fclose(image);
		is_valid = false;
	}

	virtual bool TryRecognize()
	{
		return is_valid;
	}

	virtual int GetWidth()
	{
		return cinfo.image_width;
	}

	virtual int GetHeight()
	{
		return cinfo.image_height;
	}

	virtual int GetColorDepth()
	{
		return 8;
	}

	virtual std::string GetCompressionType()
	{
		return "lossy";
	}

	virtual std::string GetFormatSpecificInfo()
	{
		std::stringstream ss;
		ss << "density unit\t";
		switch (cinfo.density_unit)
		{
			case 1:
				ss << "dots/inch\n";
				break;
			case 2:
				ss << "dots/cm\n";
				break;
			default:
				ss << "unknown\n";
		}
		ss << "X density \t" << cinfo.X_density << "\n";
		ss << "Y density \t" << cinfo.Y_density << "\n";
		for (auto tn = 0; tn < NUM_QUANT_TBLS; ++tn)
		{
			JQUANT_TBL *table = cinfo.quant_tbl_ptrs[tn];
			if (table != NULL)
			{
				ss << "quant table " << tn << "\n";
				for (int i = 0; i < 8; ++i)
				{
					for (int j = 0; j < 8; ++j)	
						ss << "\t" << table->quantval[8*i+j];
					ss << "\n";
				}
			}
		}
		return ss.str();
	}

private:
	FILE *image;
	jpeg_decompress_struct cinfo;
	MyJpegErrorManager jerr;
	bool is_valid = false;

	static jmp_buf setjmp_buffer;

	static void jpeg_error_exit_ignore(j_common_ptr cinfo)
	{
		MyJpegErrorManager* myerr = (MyJpegErrorManager*) cinfo->err;
		longjmp(myerr->setjmp_buffer, 1);
	}
};
