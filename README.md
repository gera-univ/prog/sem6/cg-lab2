Print information about images in different formats.

# Dependencies

- libpng
- libjpeg(-turbo)
- (lib)tiff
- [gifdec](https://github.com/lecram/gifdec) (included in the project, public domain)
- libavif

# Installation

```
$ meson setup builddir
$ meson compile -C builddir
$ cd builddir/
# ninja install
or
# ninja uninstall
```

# Usage

```
$ cg-lab2 ~/img/gentoo-3d.jpg
format    	jpg
dimensions	1873x1917
bit depth 	8
compression	lossy
density unit	dots/cm
X density 	37
Y density 	37
quant table 0
	3	2	2	3	4	6	8	10
	2	2	2	3	4	9	10	9
	2	2	3	4	6	9	11	9
	2	3	4	5	8	14	13	10
	3	4	6	9	11	17	16	12
	4	6	9	10	13	17	18	15
	8	10	12	14	16	19	19	16
	12	15	15	16	18	16	16	16
quant table 1
	3	3	4	8	16	16	16	16
	3	3	4	11	16	16	16	16
	4	4	9	16	16	16	16	16
	8	11	16	16	16	16	16	16
	16	16	16	16	16	16	16	16
	16	16	16	16	16	16	16	16
	16	16	16	16	16	16	16	16
	16	16	16	16	16	16	16	16
```

You can use `find` to run the program on files inside directory

```
$ find ~/img/ -type f -exec cg-lab2 {} \; -exec printf "\n\n\n" \;
```

Invoking the program without specifying file parameter will display a list
of supported formats

```
$ cg-lab2
usage: cg-lab2 image
supported image formats: jpg ff png tiff gif bmp pcx avif
```
