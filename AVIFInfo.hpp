#pragma once
#include <tiffio.h>
#include <cstdio>
#include "avif/avif.h"

class AVIFInfo : public FormatInfo {
public:
	AVIFInfo()
	{
		decoder = avifDecoderCreate();
	}

	~AVIFInfo()
	{
		avifDecoderDestroy(decoder);
	}

	virtual std::string GetFormatName()
	{
		return "avif";
	}

	virtual void Open(std::string filename)
	{
		is_valid = false;
		avifResult result = avifDecoderSetIOFile(decoder, filename.c_str());
		is_valid = result == AVIF_RESULT_OK;
		if (!is_valid) return;
		result = avifDecoderParse(decoder);
		is_valid = result == AVIF_RESULT_OK;
	}

	virtual void Close() {}

	virtual bool TryRecognize()
	{
		return is_valid;
	}

	virtual int GetWidth()
	{
		return decoder->image->width;
	}

	virtual int GetHeight()
	{
		return decoder->image->height;
	}

	virtual int GetColorDepth()
	{
		return decoder->image->depth;
	}

	virtual std::string GetCompressionType()
	{
		return "lossy";
	}

private:
	avifDecoder *decoder;
	bool is_valid = false;
};

