#pragma once

class FormatInfo
{
public:
	FormatInfo() {}
	virtual ~FormatInfo() = 0;
	virtual std::string GetFormatName() = 0;
	virtual void Open(std::string filename) = 0;
	virtual void Close() = 0;
	virtual bool TryRecognize() = 0;
	virtual	int GetWidth() = 0;
	virtual	int GetHeight() = 0;
	virtual	int GetColorDepth() = 0;
	virtual std::string GetCompressionType()
	{
		return "unknown";
	}
	virtual std::string GetFormatSpecificInfo()
	{
		return "";
	}
};

FormatInfo::~FormatInfo() {}
