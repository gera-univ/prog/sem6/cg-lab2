#pragma once

#include <fstream>
#include <cstring>
#include <arpa/inet.h>

struct FarbfeldHeader {
	char signature[8];
	uint32_t width;
	uint32_t height;
};

class Farbfeld
{
public:
	Farbfeld(std::string filename)
	{
		image.open(filename, std::ios_base::in | std::ios_base::binary);
		image.read(reinterpret_cast<char *>(&header), sizeof (FarbfeldHeader));
	}
	
	~Farbfeld()
	{
		image.close();
	}
	
	bool IsValid()
	{
		return memcmp(header.signature, "farbfeld", 8) == 0;
	}

	uint32_t GetWidth()
	{
		return ntohl(header.width);
	}

	uint32_t GetHeight()
	{
		return ntohl(header.height);
	}

private:
	std::ifstream image;
	FarbfeldHeader header;
};
