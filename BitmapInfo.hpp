#pragma once
#include <memory>
#include "FormatInfo.hpp"
#include "Bitmap.hpp"

class BitmapInfo : public FormatInfo {
public:
	BitmapInfo()
	{
	}

	virtual std::string GetFormatName()
	{
		return "bmp";
	}

	virtual void Open(std::string filename)
	{
		image = std::make_unique<Bitmap>(filename);
	}

	virtual void Close()
	{
		image.release();
	}

	virtual bool TryRecognize()
	{
		return image->IsValid();
	}

	virtual int GetWidth()
	{
		return image->GetWidth();
	}

	virtual int GetHeight()
	{
		return image->GetHeight();
	}

	virtual int GetColorDepth()
	{
		return image->GetColorDepth();
	}

	virtual std::string GetCompressionType()
	{
		return "lossless";
	}

private:
	std::unique_ptr<Bitmap> image;
};
