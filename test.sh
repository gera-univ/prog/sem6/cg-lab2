#!/bin/sh
mkdir -p examples
[ ! -f "examples/gentoo-3d.png" ] &&
curl https://www.gentoo.org/assets/img/logo/gentoo-3d.png >examples/gentoo-3d.png

extensions=(ff bmp jpg pcx tiff gif)
for format in "${extensions[@]}"
do
	[ ! -f "examples/gentoo-3d.$format" ] &&
		convert examples/gentoo-3d.png examples/gentoo-3d.$format
done

[ ! -f "examples/gentoo-3d.avif" ] &&
	avifenc examples/gentoo-3d.png examples/gentoo-3d.avif

find ./examples/ -type f -exec cg-lab2 {} \; -exec printf "\n\n\n" \;
