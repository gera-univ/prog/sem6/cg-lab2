#pragma once
#include <memory>
#include "FormatInfo.hpp"
#include "Farbfeld.hpp"

class FarbfeldInfo : public FormatInfo {
public:
	FarbfeldInfo()
	{
	}

	virtual std::string GetFormatName()
	{
		return "ff";
	}

	virtual void Open(std::string filename)
	{
		image = std::make_unique<Farbfeld>(filename);
	}

	virtual void Close()
	{
		image.release();
	}

	virtual bool TryRecognize()
	{
		return image->IsValid();
	}

	virtual int GetWidth()
	{
		return image->GetWidth();
	}

	virtual int GetHeight()
	{
		return image->GetHeight();
	}

	virtual int GetColorDepth()
	{
		return 16;
	}

	virtual std::string GetCompressionType()
	{
		return "uncompressed";
	}

private:
	std::unique_ptr<Farbfeld> image;
};

